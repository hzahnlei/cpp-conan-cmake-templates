# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for modern  C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

# ---- General setting --------------------------------------------------------

cmake_minimum_required (VERSION 3.20)

project (hexlib CXX) # CHANGE ME

# Be very strict when compiling. Try to find and eliminate all warnings.
if (MSVC)
    # warning level 4
    add_compile_options (/W4)
else ()
    # additional warnings
    add_compile_options (-Wall -Wextra -Wpedantic)
endif ()

set (CMAKE_CXX_FLAGS "-g -O0 -fprofile-arcs -ftest-coverage")
set (CMAKE_CXX_OUTPUT_EXTENSION_REPLACE ON)

# Exports a Compilation Database for use with SonnarLint
# See https://docs.sonarsource.com/sonarlint/vs-code/getting-started/running-an-analysis/#analyze-c-and-cpp-code
set (CMAKE_EXPORT_COMPILE_COMMANDS on)


# ---- The library ------------------------------------------------------------

add_subdirectory (src)


# ---- Testing ----------------------------------------------------------------

# Be advised:  The following line  cannot be part of the  CMakeLists.txt in the
# added  subdirectory for tests.  The test executable gets compiled and  linked
# and is actually executable.  But CTest will be  unable to find the  tests but
# reports "No tests were found!!!"
include (CTest)
add_subdirectory (test)


# ---- Coverage ---------------------------------------------------------------

# Use gcovr for determining the code coverage. Therefore, a custom target and a
# custom command have been defined.  These will be part of the Makefile genera-
# ted by CMake.
add_custom_target (coverage
    COMMAND mkdir -p coverage-report
    # COMMAND ${CMAKE_MAKE_PROGRAM} test
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

add_custom_command (TARGET coverage
    COMMAND gcovr --exclude="test/*.*"
                  --html-nested ${CMAKE_BINARY_DIR}/coverage-report/index.html
                  --cobertura ${CMAKE_BINARY_DIR}/coverage-report/cobertura.xml
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
