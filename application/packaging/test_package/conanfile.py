# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for modern  C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os
from conan import ConanFile
from conan.tools.build import can_run


class AppPackageTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        if can_run(self):
            self.run("app 123", env="conanrun")
