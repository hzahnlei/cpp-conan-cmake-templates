# Templates for C++ applications projects using CMake and Conan

## How to use that template

### Preparation

1. Copy the `application` template into your folder.
2. Adapt all the locations marked `CHANGE ME`.
   A lot of these changes are purely name changes to reflect the name of your library and source files.
   Some changes are adding requirements and build requirements.
3. Also rename the source file names to your needs.
   Make sure the name changes are reflected everywhere in the CMake and Conan files as well.

The folder structure is as follows:

```text
+-- artwork/                - Project logo bitmap to be used as a GitLab avatar
!
+-- models/                 - Models for generating code (optional).
!                             For example openAPI or asyncAPI.
!
+-- packaging/
!   !
!   +-- test_package/       - A package test
!   !   !
!   !   +-- src/            - Package tests (.cpp files)
!   !   !
!   !   +-- CMakeLists.txt  - CMake file for building and running the test
!   !   +-- conanfile.py    - Conan file for building and running the test    
!   !
!   +-- CMakeLists.txt      - CMake file for building the package
!   +-- conanfile.py        - Conan file for building the package
!
+-- src/                    - Contains all application source files and headers
!
+-- test/                   - (Unit) tests
!   !
!   +-- CMakeLists.txt      - Configuring the test build    
!   +-- *.cpp               - Test cases
!
+-- .clang-format           - Configuration of automated code formatting
+-- .gitignore              - Stuff you do not want to be under version control
+-- CMakeLists.txt          - Configure CMake for use with an IDE/CLI
+-- Doxyfile                - Configures the generation of source documentation
+-- conanfile.txt           - Conan dependencies to be consumed
+-- Makefile                - Helps running tests, building the package, ...
+-- README.md               - This explanatory test file
```

### Creating a Conan package (CLI)

In the most simplest case just run `make package`.
This will create a `build_package` folder with everything that is required and instruct Conan to build your package.
Running `conan list app` should output something like this

```text
Local Cache
  app
    app/1.0.0
```

Your package is now in your local Conan cache and might be pushed into a public/private package registry.

### Starting IDE development

Just type `make code` to launch Visual Studio Code.
What happens is:
- Conan will be instructed to download/build your dependencies
- CMake will be instructed to use the presets prepared by Conan
- VS Code will be instructed to use the CMake settings and Conan dependencies for your convenience
  - VS Code might ask you to pick a "configuration", pick `conan-debug`

I am using the `conan-debug` preset for IDE development because this allows me to debug my code.
Furthermore this configuration/preset generates a compilation database to be used by SonarLint, giving me hints directly in the IDE.

### Building and testing the library (CLI)

Just type `make dependencies`, `make build`, and then `make test` to download the dependencies, build the library and tests, and then execute the tests.

You may skip `make dependencies` on subsequent attempts, except the dependencies have changed.

You may also try `make clean`.
This will delete the build folder and allow for a "clean" build.
