# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for modern  C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

[requires]
fmt/10.1.1

[tool_requires]
catch2/3.4.0

[generators]
CMakeDeps
CMakeToolchain

[layout]
cmake_layout
