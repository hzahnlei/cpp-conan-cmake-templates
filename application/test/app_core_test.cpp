//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#include "app_core.hpp"
#include "version.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Integer number gets converted to hex", "fast") {
	REQUIRE(hex_representation(13) == "0x0d");
}

TEST_CASE("Version should be 1.0.13-ALPHA", "fast") {
	REQUIRE(app::VERSION_MAJOR == 1);
	REQUIRE(app::VERSION_MINOR == 0);
	REQUIRE(app::VERSION_PATCH == 13);
	REQUIRE(app::VERSION_EXTENSION == "ALPHA");
}
