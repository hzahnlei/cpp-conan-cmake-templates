//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#include "hex/hex.hpp"
#include "hex/version.hpp"
#include <catch2/catch_all.hpp>

TEST_CASE("Integer number gets converted to hex", "fast") {
	REQUIRE(hex_representation(13) == "0x0d");
}

TEST_CASE("Version should be 1.0.0-ALPHA", "fast") {
	REQUIRE(hex::VERSION_MAJOR == 1);
	REQUIRE(hex::VERSION_MINOR == 0);
	REQUIRE(hex::VERSION_PATCH == 0);
	REQUIRE(hex::VERSION_EXTENSION == "ALPHA");
}
