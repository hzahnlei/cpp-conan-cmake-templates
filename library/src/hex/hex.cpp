//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#include "hex/hex.hpp"
#include <fmt/core.h>

auto hex_representation(const unsigned int value) -> std::string {
	return fmt::format("{:#04x}", value);
}
