# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for modern  C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

include (Catch)
enable_testing ()

find_package (Catch2 REQUIRED 3)

add_executable (tests app_core_test.cpp)

target_link_libraries (tests PRIVATE
    appcore
    Catch2::Catch2WithMain)

target_include_directories (tests PRIVATE
    ${PROJECT_SOURCE_DIR}/src
    ${APP_GENERATED_DIR})

catch_discover_tests (tests REPORTER junit OUTPUT_DIR ../test-report)
