//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#pragma once

#include <string>

namespace app {
	static constexpr unsigned int VERSION_MAJOR{@APP_VERSION_MAJOR@};
	static constexpr unsigned int VERSION_MINOR{@APP_VERSION_MINOR@};
	static constexpr unsigned int VERSION_PATCH{@APP_VERSION_PATCH@};
	static const std::string VERSION_EXTENSION{"@APP_VERSION_EXTENSION@"};
} // namespace app
