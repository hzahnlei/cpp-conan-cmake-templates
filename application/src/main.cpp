//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#include "app_core.hpp"
#include <iostream>

auto main(int argc, char *argv[]) -> int {
	if (argc == 2) {
		const auto value{std::stoi(argv[1])};
		std::cout << "The hexadecimal representation of " << value
			  << " is " << hex_representation(value) << ".\n";
	} else {
		std::cout << "No argument given.\n";
	}
	return 0;
}
