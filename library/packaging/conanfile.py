# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.files import copy  # CHANGE ME may not be required, see below


class HexLibRecipe(ConanFile):  # CHANGE ME
    name = "hexlib"  # CHANGE ME
    version = "1.0.0"  # CHANGE ME
    package_type = "library"

    # Optional metadata
    license = "MIT"  # CHANGE ME
    author = "Holger Zahnleiter opensource@holger.zahnleiter.org"  # CHANGE ME
    url = "https://gitlab.com/hzahnlei/cpp-conan-cmake-templates"  # CHANGE ME
    description = (
        "hexlib - Easily convert integer values to hexadecimal strings!"  # CHANGE ME
    )
    topics = ("c++", "hexadecimal", "conversion", "conan", "cmake")  # CHANGE ME

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "models/*"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        # self.folders.source = "../src"
        # self.folders.build = "build"
        # self.cpp.source.includedirs = ["../include"]
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        # CHANGE ME If required copy all headers, not only the one denoted in CMakeLists.txt/PUBLIC_HEADER
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "include"),
            dst=os.path.join(self.package_folder, "include"),
        )
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "generated"),
            dst=os.path.join(self.package_folder, "include"),
        )
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["hexlib"]  # CHANGE ME

    def requirements(self):
        self.requires("fmt/10.1.1")  # CHANGE ME

    # CHANGE ME Add build requirements here.
    # def build_requirements(self):
    #     self.tool_requires("cmake/3.27.5")
