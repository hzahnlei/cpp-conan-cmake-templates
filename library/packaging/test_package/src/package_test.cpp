//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#include <hex/hex.hpp>
#include <iostream>
#include <string>

auto main() -> int
{
	static constexpr auto value{13U};
	std::cout << "The hex representation of " << value << " is " << hex_representation(value) << ".\n\n";
	std::cout << "Major version: " << hex::VERSION_MAJOR << '\n';
	std::cout << "Minor version: " << hex::VERSION_MINOR << '\n';
	std::cout << "Patch level: " << hex::VERSION_PATCH << '\n';
	std::cout << "Extension: " << hex::VERSION_EXTENSION << '\n';
}
