//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#pragma once

#include "hex/version.hpp"
#include <string>

#ifdef _WIN32
#define HEX_LIB_EXPORT __declspec(dllexport)
#else
#define HEX_LIB_EXPORT
#endif

HEX_LIB_EXPORT auto hex_representation(const unsigned int) -> std::string;
