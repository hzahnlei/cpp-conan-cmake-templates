//******************************************************************************
//
// cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
// cies managed by Conan, and Make/CMake used as the build tool.
//
// (c) 2019, 2023 Holger Zahnleiter, All rights reserved
//
//*****************************************************************************

#pragma once

#include <string>

#ifdef _WIN32
#define HELLO_EXPORT __declspec(dllexport)
#else
#define HELLO_EXPORT
#endif

HELLO_EXPORT auto hex_representation(const unsigned int) -> std::string;
