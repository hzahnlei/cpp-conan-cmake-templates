# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for modern  C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

cmake_minimum_required (VERSION 3.20)
project (PackageTest CXX)

find_package (hexlib CONFIG REQUIRED) # CHANGE ME
# CHANGE ME add required packages, if applicable
# find_package(fmt CONFIG REQUIRED)

add_executable (package-test src/package_test.cpp) # CHANGE ME
target_link_libraries (package-test hexlib::hexlib) # CHANGE ME
